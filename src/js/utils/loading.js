import React, { Fragment, Component, useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Loading from 'react-top-loading-bar';

import { startLoad, endLoad, clearLoad } from '../reducers/clientReducer';

class LoadingBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  static getDerivedStateFromProps(nextProps, prevState) {
    return {};
  }

  render() {
    return (
      <Loading
        height={3}
        color='#FF4D4D'
        progress={this.props.loading}
        onLoaderFinished={() => this.props.clearLoad()}
      />
    );
  }
}

const mapStateToProps = state => ({
  loading: state.client.loading,
});

export default connect(
  mapStateToProps,
  {
    startLoad,
    endLoad,
    clearLoad,
  },
)(LoadingBar);
