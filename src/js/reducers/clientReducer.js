import { apiRequest as axios } from '../utils/apiRequest';

export const START_LOADING = 'START_LOADING';
export const END_LOADING = 'END_LOADING';
export const CLEAR_LOADING = 'CLEAR_LOADING';

const initialState = {
  loading: 0,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case START_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    case END_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    case CLEAR_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    default:
      return state;
  }
}

export const startLoad = props => dispatch => {
  dispatch({
    type: START_LOADING,
    payload: 30,
  });
};

export const endLoad = props => dispatch => {
  dispatch({
    type: START_LOADING,
    payload: 100,
  });
};

export const clearLoad = props => dispatch => {
  dispatch({
    type: CLEAR_LOADING,
    payload: 0,
  });
};
