import axios from 'axios';
import { urlApi } from '../urls.js';

// import { apiRequest as axios } from '../utils/apiRequest';

export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const OPEN_AUTH = 'OPEN_AUTH';
export const OPEN_PROFILE = 'OPEN_PROFILE';
export const CLEAR_USER = 'CLEAR_USER';
export const OPEN_CART = 'OPEN_CART';
export const ADD_CART = 'ADD_CART';

const initialState = {
  user: null,
  openAuth: false,
  openProfile: false,
  openCart: false,
  cart: [],
  price: 0,
  count: 0,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        user: action.payload,
        openAuth: false,
      };
    case CLEAR_USER:
      return {
        ...state,
        user: null,
        openAuth: false,
      };
    case OPEN_AUTH:
      return {
        ...state,
        openAuth: action.payload,
      };
    case OPEN_PROFILE:
      return {
        ...state,
        openProfile: action.payload,
      };
    case OPEN_CART:
      return {
        ...state,
        openCart: action.payload,
      };
    case ADD_CART:
      return {
        ...state,
        cart: [...state.cart, action.payload],
        price:
          state.price + action.payload.product.price * action.payload.count,
        count: state.count + action.payload.count,
      };
    default:
      return state;
  }
}

export const showAuth = props => dispatch => {
  dispatch({
    type: OPEN_AUTH,
    payload: props,
  });
};

export const showProfile = props => dispatch => {
  dispatch({
    type: OPEN_PROFILE,
    payload: props,
  });
};

export const showCart = props => dispatch => {
  dispatch({
    type: OPEN_CART,
    payload: props,
  });
};

export const addCart = props => (dispatch, getState) => {
  // axios
  //   .post(`${urlApi}/orders/basket/`, {
  //     amount: props.count,
  //     item: props.product,
  //   })
  //   .then(response => {
  //     dispatch({
  //       type: ADD_CART,
  //       payload: props,
  //     });
  //   })
  //   .catch(error => {
  //     console.log(error.data);
  //   });

  dispatch({
    type: ADD_CART,
    payload: props,
  });
};

export const register = props => dispatch => {
  axios
    .post(`${urlApi}/auth/register/`, props)
    .then(response => {
      localStorage.setItem('tkn', response.key);
      dispatch({ type: SET_USER, payload: response.data });
    })
    .catch(error => {
      console.log(error.data);
    });
};

export const logout = props => dispatch => {
  axios
    .post(`${urlApi}/auth/logout/`, props)
    .then(response => {
      dispatch({ type: CLEAR_USER });
    })
    .catch(error => {
      console.log(error.data);
    });
};

export const login = props => dispatch => {
  axios
    .post(`${urlApi}/auth/login/`, props, { withCredentials: true })
    .then(response => {
      console.log(response);
      localStorage.setItem('tkn', response.data.key);
      dispatch({ type: SET_CURRENT_USER, payload: response.data.user });
    })
    .catch(error => {
      console.log(error.data);
    });
};

export const setCurrentUser = token => dispatch => {
  axios
    .get(`${urlApi}/auth/user/`)
    .then(response => {
      dispatch({
        type: SET_CURRENT_USER,
        payload: response.data,
      });
    })
    .catch(function(error) {
      console.log(error.response);
    });
};
