import { combineReducers } from 'redux';
import authReducer from './authReducer';
import goodsReducer from './goodsReducer';
import clientReducer from './clientReducer';

export default combineReducers({
  auth: authReducer,
  product: goodsReducer,
  client: clientReducer,
});
