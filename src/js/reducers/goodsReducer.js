import { apiRequest as axios } from '../utils/apiRequest';
import { urlApi } from '../urls.js';

export const SET_CURRENT_GOODS = 'SET_CURRENT_GOODS';
export const SET_CURRENT_GOOD = 'SET_CURRENT_GOOD';
export const SET_NEW_GOODS = 'SET_NEW_GOODS';
export const CLEAR_PRODUCTS = 'CLEAR_PRODUCTS';
export const CLEAR_PRODUCT = 'CLEAR_PRODUCT';
export const SET_CURRENT_CATEGORIES = 'SET_CURRENT_CATEGORIES';

const initialState = {
  products: [],
  product: null,
  new: [],
  cart: [],
  categories: [],
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_GOODS:
      return {
        ...state,
        products: action.payload.results,
      };

    case SET_CURRENT_GOOD:
      return {
        ...state,
        product: action.payload,
      };
    case SET_NEW_GOODS:
      return {
        ...state,
        new: action.payload.results,
      };
    case CLEAR_PRODUCTS:
      return {
        ...state,
        products: [],
        new: [],
      };
    case SET_CURRENT_CATEGORIES:
      return {
        ...state,
        categories: action.payload,
      };
    case CLEAR_PRODUCT:
      return {
        ...state,
        product: null,
      };
    default:
      return state;
  }
}

export const getProduct = id => dispatch => {
  axios
    .get(`${urlApi}/goods/${id}/`)
    .then(response => {
      dispatch({ type: SET_CURRENT_GOOD, payload: response.data });
    })
    .catch(error => {
      console.log(error.data);
    });
};

export const addCart = props => dispatch => {};

export const getProducts = (params = {}) => dispatch => {
  axios
    .get(`${urlApi}/goods/`, { params: params })
    .then(response => {
      dispatch({ type: SET_CURRENT_GOODS, payload: response.data });
    })
    .catch(error => {
      console.log(error.data);
    });
};

export const getCategories = () => dispatch => {
  axios
    .get(`${urlApi}/goods/categories/`)
    .then(response => {
      dispatch({
        type: SET_CURRENT_CATEGORIES,
        payload: response.data.categories[0].subcategories,
      });
    })
    .catch(error => {
      console.log(error.data);
    });
};

export const getNewProducts = () => dispatch => {
  axios
    .get(`${urlApi}/goods/`, {
      params: {
        is_new: true,
      },
    })
    .then(response => {
      dispatch({ type: SET_NEW_GOODS, payload: response.data });
    })
    .catch(error => {
      console.log(error.data);
    });
};

export const clearProducts = () => dispatch => {
  dispatch({ type: CLEAR_PRODUCTS });
};

export const clearProduct = () => dispatch => {
  dispatch({ type: CLEAR_PRODUCT });
};
