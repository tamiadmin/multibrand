import React, { Fragment, Component, useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import './goods.css';

import { apiRequest as axios } from '../../utils/apiRequest';
import ProductItem from '../../components/product-item';

import { getProduct } from '../../reducers/goodsReducer';
import { urlApi } from '../../urls.js';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterFade: {
        price: false,
      },
      filter: {
        priceMin: 0,
        priceMax: 0,
        creator: [],
        categories: [],
      },
      products: [],
      categories: [],
      product: null,
      next: null,
      previous: null,
    };
    this.minChange = this.minChange.bind(this);
    this.maxChange = this.maxChange.bind(this);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }

  componentDidMount() {
    axios
      .get(`${urlApi}/goods/`)
      .then(response => {
        console.log(response.data);
        this.setState({
          products: response.data.results,
          next: response.data.next,
          previous: response.data.previous,
        });
        axios
          .get(`${urlApi}/goods/${response.data.results[0].id}/`)
          .then(response => {
            this.setState({
              product: response.data,
            });
          })
          .catch(error => {
            console.log(error.data);
          });
      })
      .catch(error => {
        console.log(error.data);
      });
  }

  minChange(e) {
    this.setState(prevState => ({
      filter: {
        ...prevState.filter,
        priceMin: e.currentTarget.value,
        priceMax:
          prevState.filter.priceMax < e.currentTarget.value
            ? e.currentTarget.value
            : prevState.filter.priceMax,
      },
    }));
  }

  componentDidUpdate() {
    if (this.state.product !== null && this.state.categories.length === 0) {
      var cat = this.setCategories(this.state.product.category);
      this.setState({
        categories: cat,
      });
    }
  }

  setCategories(e) {
    let cat = [];
    cat.push(e.name);
    var depth = e.of;
    for (var i = 0; i < 10; i++) {
      name = depth.name;
      if (!depth.of) {
        cat.push(name);
        return cat.reverse();
      }
      depth = depth.of;

      cat.push(name);
    }
    return cat.reverse();
  }

  maxChange(e) {
    this.setState(prevState => ({
      filter: {
        ...prevState.filter,
        priceMax: e.currentTarget.value,
        priceMin:
          prevState.filter.priceMin > e.currentTarget.value
            ? e.currentTarget.value
            : prevState.filter.priceMin,
      },
    }));
  }

  next() {
    axios
      .get(this.state.next)
      .then(response => {
        this.setState({
          products: response.data.results,
          next: response.data.next,
          previous: response.data.previous,
        });
      })
      .catch(error => {
        console.log(error.data);
      });
  }

  previous() {
    axios
      .get(this.state.previous)
      .then(response => {
        this.setState({
          products: response.data.results,
          next: response.data.next,
          previous: response.data.previous,
        });
      })
      .catch(error => {
        console.log(error.data);
      });
  }

  render() {
    let filterFade = this.state.filterFade;
    let filter = this.state.filter;

    return (
      <Fragment>
        <div className='goods'>
          <div className='container'>
            <div className='goods__inner'>
              <ul className='product-path uk-breadcrumb'>
                <li>
                  <a href='#'>Главная</a>
                </li>
                <li>
                  <a href='#'>Продукция</a>
                </li>
              </ul>
              <div className='goods-title'>Все продукты</div>
              <div className='goods-content'>
                <div className='goods-filter'>
                  <div className='goods-filter-i'>
                    <div className='goods-filter-i-tilte'>
                      <img
                        src='../public/images/icons/filter-show.svg'
                        alt=''
                        className={
                          filterFade.price
                            ? 'goods-filter-i-arrow goods-filter-i-arrow-active'
                            : 'goods-filter-i-arrow'
                        }
                        onClick={() => {
                          this.setState(prevState => ({
                            filterFade: {
                              ...filterFade,
                              price: !prevState.filterFade.price,
                            },
                          }));
                        }}
                      />
                      Цена
                    </div>
                    <div
                      className={
                        filterFade.price
                          ? 'goods-filter-i-content goods-filter-i-price fade'
                          : 'none'
                      }
                    >
                      <input
                        type='number'
                        placeholder='от'
                        value={this.state.filter.priceMin}
                        onChange={e => this.maxChange(e)}
                      />
                      <span>-</span>
                      <input
                        type='number'
                        placeholder='до'
                        value={this.state.filter.priceMax}
                        onChange={e => this.minChange(e)}
                      />
                    </div>
                  </div>
                </div>
                <div className='goods-items'>
                  <div className='goods-items-inner'>
                    {this.state.products.map((i, key) => {
                      return <ProductItem width={true} key={key} product={i} />;
                    })}
                    <div className='goods-items-pag'>
                      {this.state.previous ? (
                        <span onClick={() => this.previous()}>Назад</span>
                      ) : null}
                      {this.state.next ? (
                        <span onClick={() => this.next()}>Далее</span>
                      ) : null}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.product.categories,
});

export default connect(mapStateToProps)(Search);
