import React, { Fragment, Component, useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Menu, Icon, Dropdown } from 'antd';
import 'antd/lib/menu/style/index.css';

import Auth from '../../components/auth';
import Profile from '../../components/profile';
import Cart from '../../components/cart';

import {
  showAuth,
  addCart,
  setCurrentUser,
  showProfile,
  showCart,
} from '../../reducers/authReducer';

import './header.css';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
    };
    // this.change = this.change.bind(this);
  }

  componentDidMount() {
    if (this.props.user === null) {
      console.log('de');
      this.props.setCurrentUser();
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    console.log(nextProps);
    if (nextProps.user) {
      return {
        user: nextProps.user,
      };
    }
    return {};
  }

  render() {
    const { SubMenu } = Menu;
    return (
      <Fragment>
        <Auth />
        <Profile />
        <Cart />
        <div
          className='header'
          // data-uk-sticky="show-on-up: true; animation: uk-animation-slide-top;"
        >
          <div className='container'>
            <div className='header__inner'>
              <div className='header-top'>
                <Link to='/' className='header-logo'>
                  <img
                    src='../public/images/logo.png'
                    alt='multibrand логотип'
                  />
                </Link>
                <div className='header-address'>Г.Aлматы,ул.Байзакова,280</div>
                <div className='header-search'>
                  <input
                    className='header-search-input'
                    type='text'
                    placeholder='Поиск товаров ...'
                  />
                  <div className='header-search-icon'>
                    <img src='../public/images/icons/search.svg' />
                  </div>
                </div>
                <div className='header-profile'>
                  <div className='header-profile-main'>
                    {this.props.user ? (
                      <>
                        <div className='header-profile-i'>
                          <img
                            src='../public/images/icons/avatar.svg'
                            alt='Профиль'
                            onClick={() => this.props.showProfile(true)}
                          />
                        </div>
                        <div className='header-profile-div'></div>
                        <div className='header-profile-i'>
                          <img
                            src='../public/images/icons/heart.svg'
                            alt='Понравилось'
                          />
                        </div>
                        <div className='header-profile-div'></div>
                        <div className='header-profile-i'>
                          <img
                            src='../public/images/icons/eye.svg'
                            alt='Просмотры'
                          />
                        </div>
                      </>
                    ) : (
                      <div className='header-profile-auth'>
                        <span onClick={() => this.props.showAuth(true)}>
                          Войти
                        </span>
                        <span onClick={() => this.props.showAuth(true)}>
                          Регистрация
                        </span>
                      </div>
                    )}
                  </div>
                  <div
                    className='header-profile-cart'
                    onClick={() => {
                      this.props.showCart(true);
                    }}
                  >
                    <div className='header-profile-cart-icon'>
                      <img
                        src='../public/images/icons/cart.svg'
                        alt='Корзина'
                      />
                    </div>
                    <div className='header-profile-cart-text'>
                      <div className='header-profile-cart-title'>Корзина</div>
                      <div className='header-profile-cart-value'>
                        {this.props.auth.count}шт | {this.props.auth.price} тг
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className='header-bottom'>
                {/* {this.props.categories.length > 0 ? (
                  <Dropdown
                    overlay={
                      <Menu mode='vertical'>
                        <Menu.Item>
                          <Link to={'/goods'}>Новинки</Link>
                        </Menu.Item>
                        <Menu.Item>
                          <Link to={'/goods'}>Акции</Link>
                        </Menu.Item>
                        <Menu.Item>
                          <Link to={'/goods'}>Популярное</Link>
                        </Menu.Item>
                        {this.props.categories.map((i, key) => {
                          return (
                            <SubMenu
                              key={i.id}
                              title={
                                <span>
                                  <Link to={'/goods/' + i.id}>{i.name}</Link>
                                </span>
                              }
                            >
                              {i.subcategories.map((sub, k) => {
                                return (
                                  <SubMenu
                                    key={sub.id}
                                    title={
                                      <span>
                                        <Link to={'/goods/' + sub.id}>
                                          {sub.name}
                                        </Link>
                                      </span>
                                    }
                                  >
                                    {sub.subcategories.map((item, ke) => {
                                      return (
                                        <Menu.Item key={item.id}>
                                          <Link to={'/goods/' + item.id}>
                                            {item.name}
                                          </Link>
                                        </Menu.Item>
                                      );
                                    })}
                                  </SubMenu>
                                );
                              })}
                            </SubMenu>
                          );
                        })}
                      </Menu>
                    }
                  > */}
                <div className='header-menu ant-dropdown-link'>
                  <img
                    className='header-menu-icon'
                    src='../public/images/icons/menu.svg'
                    alt='menu'
                  />
                  Каталог товаров
                </div>
                {/* </Dropdown>
                ) : null} */}

                <div className='header-nav'>
                  <Link to='/payment' className='header-nav-i'>
                    Доставка и Оплата
                  </Link>
                  <Link to='/payment' className='header-nav-i'>
                    О Компании
                  </Link>
                  <Link to='/payment' className='header-nav-i'>
                    Отзывы
                  </Link>
                  <Link to='/payment' className='header-nav-i'>
                    Контакты
                  </Link>
                </div>
                <div className='header-phone'>
                  <a href='tel:+77473039758'>8 (747) 303-97-58</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  auth: state.auth,
  categories: state.product.categories,
});

export default connect(
  mapStateToProps,
  {
    addCart,
    showAuth,
    setCurrentUser,
    showProfile,
    showCart,
  },
)(Header);
