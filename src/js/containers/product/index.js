import React, { Fragment, Component, useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import ReactMarkdown from 'react-markdown';
import renderHTML from 'react-render-html';

import './product.css';

import { urlApi } from '../../urls.js';
import { apiRequest as axios } from '../../utils/apiRequest';

import ProductItem from '../../components/product-item';

import { startLoad, endLoad } from '../../reducers/clientReducer';
import {
  getProduct,
  getNewProducts,
  getProducts,
} from '../../reducers/goodsReducer';
import { addCart } from '../../reducers/authReducer';

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: null,
      depthCat: 0,
      categories: [],
      count: 1,
      products: [],
      new: [],
      added: false,
    };
    this.setCategories = this.setCategories.bind(this);
    this.changeCount = this.changeCount.bind(this);
    this.changeCountInput = this.changeCountInput.bind(this);
  }

  componentDidMount() {
    startLoad();
    this.props.getProduct(this.props.match.params.id, this.props.endLoad);
    this.props.getProducts();
    this.props.getNewProducts();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.cart.length > 0) {
      if (nextProps.cart.find(o => o.product.id === nextProps.product.id)) {
        return {
          added: true,
        };
      }
    }
    if (
      nextProps.products.length > 0 &&
      nextProps.products !== prevState.products
    )
      return {
        products: nextProps.new.slice(0, 4),
      };
    if (nextProps.new.length > 0 && nextProps.new !== prevState.new)
      return { new: nextProps.new.slice(0, 4) };
    if (nextProps.product !== null) {
      return { product: nextProps.product };
    }
    return {};
  }

  componentDidUpdate() {
    if (this.state.product !== null && this.state.categories.length === 0) {
      var cat = this.setCategories(this.state.product.category);
      this.setState({
        categories: cat,
      });
      this.props.endLoad();
    }
  }

  setCategories(e) {
    console.log(e);
    let cat = [];
    cat.push(e.name);
    var depth = e.of;
    for (var i = 0; i < 10; i++) {
      name = depth.name;
      if (!depth.of) {
        cat.push(name);
        return cat.reverse();
      }
      depth = depth.of;

      cat.push(name);
    }
    return cat.reverse();
  }

  changeCount(e) {
    if (e) {
      this.setState(prevState => ({
        count: prevState.count + 1,
      }));
    } else {
      if (this.state.count > 1) {
        this.setState(prevState => ({
          count: prevState.count - 1,
        }));
      }
    }
  }

  changeCountInput(e) {
    this.setState({
      count: e.target.value,
    });
  }

  addCart() {
    this.setState({});
    this.props.addCart({
      count: this.state.count,
      product: this.props.product,
    });
  }

  render() {
    const product = this.state.product;
    const input = '# This is a header\n\nAnd this is a paragraph';
    console.log(this.state);
    if (!product) {
      return null;
    }
    return (
      <Fragment>
        <div className='product'>
          <div className='container'>
            <div className='product__inner'>
              <ul className='product-path uk-breadcrumb'>
                <li>
                  <a href='#'>Главная</a>
                </li>
                {this.state.categories.map((i, k) => {
                  return (
                    <li key={k}>
                      <a href='#'>{i}</a>
                    </li>
                  );
                })}
              </ul>
              <div className='product-content'>
                <div className='product-title'>
                  <div className='pruduct-title-name'>{product.name}</div>
                  <div className='product-title-buttons'>
                    <div className='product-title-button product-title-button-share'>
                      <img src='../public/images/icons/share.svg' alt='' />
                    </div>
                    <div className='product-title-button product-title-button-heart'>
                      <img src='../public/images/icons/like-dark.svg' alt='' />
                    </div>
                  </div>
                </div>
                <div className='product-info'>
                  <div className='product-imgs'>
                    <div className='product-img'>
                      <img src={product.image} alt='' />
                    </div>
                    {/* <div className='product-imgs-links'>
                      <div className='product-imgs-link'>
                        <img
                          src='https://images-na.ssl-images-amazon.com/images/G/01/img15/brawner/AirPods_ChargingCase_newmodel_240x160._CB467726116_.png'
                          alt=''
                        />
                      </div>
                      <div className='product-imgs-link'>
                        <img
                          src='https://images-na.ssl-images-amazon.com/images/G/01/img15/brawner/AirPods_ChargingCase_newmodel_240x160._CB467726116_.png'
                          alt=''
                        />
                      </div>
                      <div className='product-imgs-link'>
                        <img
                          src='https://images-na.ssl-images-amazon.com/images/G/01/img15/brawner/AirPods_ChargingCase_newmodel_240x160._CB467726116_.png'
                          alt=''
                        />
                      </div>
                      <div className='product-imgs-link'>
                        <img
                          src='https://images-na.ssl-images-amazon.com/images/G/01/img15/brawner/AirPods_ChargingCase_newmodel_240x160._CB467726116_.png'
                          alt=''
                        />
                      </div>
                    </div> */}
                  </div>
                  <div className='product-description'>
                    <div className='product-d-title'>Описание</div>
                    <div className='product-d-info'>{product.desc}</div>
                    <div className='product-d-desc'>
                      <div className='product-d-i'>
                        <span>Наличие: </span>{' '}
                        {product.amount > 0 ? 'В наличии' : 'Нет'}
                      </div>
                      <div className='product-d-i'>
                        <span>Доставка: </span> в течении 24 часов
                      </div>
                    </div>
                  </div>
                  <div className='product-order'>
                    <div className='product-price'>
                      <div className='product-price-count'>
                        <div
                          className='product-price-count-minus'
                          onClick={() => this.changeCount(false)}
                        >
                          -
                        </div>
                        <div className='product-price-count-num'>
                          <input
                            type='text'
                            value={this.state.count}
                            onChange={e => this.changeCountInput(e)}
                          />
                        </div>
                        <div
                          className='product-price-count-plus'
                          onClick={() => this.changeCount(true)}
                        >
                          +
                        </div>
                      </div>
                      <div className='product-price-actual'>
                        {product.price} ₸
                      </div>
                    </div>
                    {this.state.added ? (
                      <div className='product-order-cart roduct-order-cart-active '>
                        В корзине
                      </div>
                    ) : (
                      <div
                        className='product-order-cart'
                        onClick={() =>
                          this.props.addCart({
                            count: this.state.count,
                            product: this.state.product,
                          })
                        }
                      >
                        В корзину
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div className='product-full'>
                <div className='product-full-menu'>
                  <div className='product-full-menu-i product-full-menu-i-active'>
                    Описание
                  </div>
                  <div className='product-full-menu-i'>Доставка</div>
                  <div className='product-full-menu-i'>Оплата</div>
                  <div className='product-full-menu-i'>Отзывы</div>
                  {/* <div className="product-full-menu-i">Дополнительно</div> */}
                </div>
                <div className='product-full-info'>
                  {product.body === '' ? (
                    <div className='product-full-info-title'>
                      Пока про этот продукт нету подробной информации
                    </div>
                  ) : (
                    <div className='product-full-info-title'>
                      {renderHTML(product.body)}
                    </div>
                  )}
                </div>
              </div>
              <div className='main-rules'>
                <div className='main-rules-i'>
                  <div className='main-rules-img'>
                    <img src='../public/images/icons/shipped.svg' alt='' />
                  </div>
                  <div className='main-rules-title'>Удобная доставка</div>
                  <div className='main-rules-text'>
                    Мы работаем с провереными транспротными компаниями
                  </div>
                </div>
                <div className='main-rules-div'></div>
                <div className='main-rules-i'>
                  <div className='main-rules-img'>
                    <img src='../public/images/icons/warehouse.svg' alt='' />
                  </div>
                  <div className='main-rules-title'>Удобная доставка</div>
                  <div className='main-rules-text'>
                    Мы работаем с провереными транспротными компаниями
                  </div>
                </div>
                <div className='main-rules-div'></div>
                <div className='main-rules-i'>
                  <div className='main-rules-img'>
                    <img src='../public/images/icons/wallet.svg' alt='' />
                  </div>
                  <div className='main-rules-title'>Удобная доставка</div>
                  <div className='main-rules-text'>
                    Мы работаем с провереными транспротными компаниями
                  </div>
                </div>
                <div className='main-rules-div'></div>
                <div className='main-rules-i'>
                  <div className='main-rules-img'>
                    <img src='../public/images/icons/diploma.svg' alt='' />
                  </div>
                  <div className='main-rules-title'>Удобная доставка</div>
                  <div className='main-rules-text'>
                    Мы работаем с провереными транспротными компаниями
                  </div>
                </div>
                <div className='main-rules-div'></div>
                <div className='main-rules-i'>
                  <div className='main-rules-img'>
                    <img src='../public/images/icons/call-center.svg' alt='' />
                  </div>
                  <div className='main-rules-title'>Удобная доставка</div>
                  <div className='main-rules-text'>
                    Мы работаем с провереными транспротными компаниями
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='main-offer product-offer'>
            <div className='main-offer-body '>
              <div className='container'>
                <div className='main-offer-inner'>
                  <div className='product-offer-title'>
                    <div className='product-offer-title-text'>
                      Новые продукты
                    </div>
                    <div className='product-offer-title-button'>
                      <Link to={'/goods'}>Показать еще</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='container'>
            <div className='product-inner'>
              <div className='main-list main-new'>
                <div className='main-list-items'>
                  {this.state.new.map((i, key) => {
                    return <ProductItem key={key} product={i} />;
                  })}
                </div>
              </div>
            </div>
          </div>

          <div className='main-offer product-offer-green product-offer'>
            <div className='main-offer-body '>
              <div className='container'>
                <div className='main-offer-inner'>
                  <div className='product-offer-title'>
                    <div className='product-offer-title-text'>Популярное</div>
                    <div className='product-offer-title-button'>
                      <Link to={'/goods'}>Показать еще</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='container'>
            <div className='product-inner'>
              <div className='main-list main-new'>
                <div className='main-list-items'>
                  {this.state.products.map((i, key) => {
                    return <ProductItem key={key} product={i} />;
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  product: state.product.product,
  products: state.product.products,
  new: state.product.new,
  cart: state.auth.cart,
});

export default connect(
  mapStateToProps,
  {
    startLoad,
    addCart,
    endLoad,
    getProduct,
    getNewProducts,
    getProducts,
  },
)(Product);
