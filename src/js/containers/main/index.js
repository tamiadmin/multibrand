import React, { Fragment, Component, useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Menu, Icon } from 'antd';

import 'antd/lib/menu/style/index.css';

import './landing.css';
import axios from 'axios';

import { urlApi } from '../../urls.js';
import ProductItem from '../../components/product-item';

import { startLoad, endLoad } from '../../reducers/clientReducer';
import {
  getProducts,
  getNewProducts,
  clearProducts,
} from '../../reducers/goodsReducer';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      new: [],
      count: 0,
      categories: [],
    };
  }

  componentDidMount() {
    this.props.startLoad();
    this.props.getProducts();
    this.props.getNewProducts();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    
    if (
      nextProps.products.length > 0 &&
      nextProps.products !== prevState.products
    )
      return {
        products: nextProps.new.slice(0, 4),
      };
    if (nextProps.new.length > 0 && nextProps.new !== prevState.new)
      return { new: nextProps.new.slice(0, 4) };
    if (
      nextProps.categories.length > 0 &&
      nextProps.categories !== prevState.categories
    )
      return { categories: nextProps.categories };
    return {};
  }

  componentDidUpdate() {
    if (this.state.new.length > 0 && this.state.products.length > 0) {
      this.props.endLoad();
    }
  }

  componentWillUnmount() {
    this.props.clearProducts();
  }

  render() {
    const { SubMenu } = Menu;
    return (
      <Fragment>
        <div className='main'>
          <div className='container'>
            <div className='main__inner'>
              <div className='main-top'>
                <div className='main-sidebar'>
                  <Menu mode='vertical'>
                    <Menu.Item>
                      <Link to={'/goods'}>Новинки</Link>
                    </Menu.Item>
                    <Menu.Item>
                      <Link to={'/goods'}>Акции</Link>
                    </Menu.Item>
                    <Menu.Item>
                      <Link to={'/goods'}>Популярное</Link>
                    </Menu.Item>
                    {this.props.categories.map((i, key) => {
                      return (
                        <SubMenu
                          key={i.id}
                          title={
                            <span>
                              <Link to='/'>{i.name}</Link>
                            </span>
                          }
                        >
                          {i.subcategories.map((sub, k) => {
                            return (
                              <SubMenu
                                key={sub.id}
                                title={
                                  <span>
                                    <Link to='/'>{sub.name}</Link>
                                  </span>
                                }
                              >
                                {sub.subcategories.map((item, ke) => {
                                  return (
                                    <Menu.Item key={item.id}>
                                      <Link to={'/goods/' + item.id}>
                                        {item.name}
                                      </Link>
                                    </Menu.Item>
                                  );
                                })}
                              </SubMenu>
                            );
                          })}
                        </SubMenu>
                      );
                    })}
                  </Menu>
                </div>
                <div className='main-slider'>
                  <div className='main-slider-top'>
                    <div className='main-slider-content'>
                      <div
                        className='uk-position-relative uk-visible-toggle uk-light main-slider-inner'
                        data-tabindex='-1'
                        data-uk-slideshow='animation: push;autoplay:true;autoplay-interval:5000'
                      >
                        <ul className='uk-slideshow-items main-slider-items'>
                          <li>
                            <img src='../public/images/example/1.png' alt='' />
                          </li>
                          <li>
                            <img src='../public/images/example/2.png' alt='' />
                          </li>
                          <li>
                            <img src='../public/images/example/3.png' alt='' />
                          </li>
                          <li>
                            <img src='../public/images/example/4.png' alt='' />
                          </li>
                        </ul>

                        <a
                          className='uk-position-center-left uk-position-small uk-hidden-hover'
                          href='#'
                          data-uk-slidenav-previous
                          data-uk-slideshow-item='previous'
                        >
                          {/* <img src="../public/images/left-arrow.svg" alt="" /> */}
                        </a>
                        <a
                          className='uk-position-center-right uk-position-small uk-hidden-hover'
                          href='#'
                          data-uk-slidenav-next
                          data-uk-slideshow-item='next'
                        >
                          {/* <img src="../public/images/right-arrow.svg" alt="" /> */}
                        </a>
                      </div>
                    </div>
                    <div className='main-slider-special'>
                      <div className='main-slider-special-inner'>
                        <div className='main-slider-special-img'>
                          <img
                            src='http://35.246.29.253/media/items/2019/09/11/6e25f78917f69bd91b6c03f54645e8e8.jpg'
                            alt=''
                          />
                        </div>
                        <div className='main-slider-special-content'>
                          <div className='main-slider-special-rating'></div>
                          <div className='main-slider-special-title'>
                            Бизнес-блокнот "Work time"
                          </div>
                          <div className='main-slider-special-price'>
                            <div className='main-slider-special-sale'>
                              1450.00 тг
                            </div>
                            1250.00 ₸
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='main-slider-bottom'>
                    <div className='main-slider-bottom-i'>
                      <img src='../public/images/icons/new.svg' alt='' />
                      <div className='main-slider-bottom-info'>
                        <div className='main-slider-bottom-title'>Новинки</div>
                        <div className='main-slider-bottom-count'>
                          более 500 товаров
                        </div>
                      </div>
                    </div>
                    <div className='main-slider-bottom-div'></div>
                    <div className='main-slider-bottom-i'>
                      <img src='../public/images/icons/sale.svg' alt='' />
                      <div className='main-slider-bottom-info'>
                        <div className='main-slider-bottom-title'>Акции</div>
                        <div className='main-slider-bottom-count'>
                          более 799 товаров
                        </div>
                      </div>
                    </div>
                    <div className='main-slider-bottom-div'></div>
                    <div className='main-slider-bottom-i'>
                      <img src='../public/images/icons/popular.svg' alt='' />
                      <div className='main-slider-bottom-info'>
                        <div className='main-slider-bottom-title'>Хиты</div>
                        <div className='main-slider-bottom-count'>
                          более 3000 товаров
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className='main-list main-new'>
                <div className='main-list-title'>
                  <div className='main-list-title-text'>
                    Новинки и <span>спецпредложения</span>
                  </div>
                  <div className='main-list-title-more'>Показать все</div>
                </div>
                <div className='main-list-items'>
                  {this.state.new.map((i, key) => {
                    return <ProductItem key={key} product={i} />;
                  })}
                </div>
              </div>
            </div>
          </div>
          <div className='main-offer'>
            <div className='main-offer-body'>
              <div className='container'>
                <div className='main-offer-inner'>
                  <div className='main-offer-text'>
                    Доставка в течение 3 часов
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='container'>
            <div className='main-list main-new'>
              <div className='main-list-title'>
                <div className='main-list-title-text'>Хиты продаж</div>
                <div className='main-list-title-more'>Показать все</div>
              </div>
              <div className='main-list-items'>
                {this.state.products.map((i, key) => {
                  return <ProductItem key={key} product={i} />;
                })}
              </div>
            </div>
            <div className='main-rules'>
              <div className='main-rules-i'>
                <div className='main-rules-img'>
                  <img src='../public/images/icons/shipped.svg' alt='' />
                </div>
                <div className='main-rules-title'>Удобная доставка</div>
                <div className='main-rules-text'>
                  Доставляем в любую точку Алматы
                </div>
              </div>
              <div className='main-rules-div'></div>
              <div className='main-rules-i'>
                <div className='main-rules-img'>
                  <img src='../public/images/icons/warehouse.svg' alt='' />
                </div>
                <div className='main-rules-title'>Склады</div>
                <div className='main-rules-text'>
                  Вся продукция превозиться с защищенных складов
                </div>
              </div>
              <div className='main-rules-div'></div>
              <div className='main-rules-i'>
                <div className='main-rules-img'>
                  <img src='../public/images/icons/wallet.svg' alt='' />
                </div>
                <div className='main-rules-title'>Способы оплаты</div>
                <div className='main-rules-text'>
                  Оплачивайте любым удобным способом оплаты
                </div>
              </div>
              <div className='main-rules-div'></div>
              <div className='main-rules-i'>
                <div className='main-rules-img'>
                  <img src='../public/images/icons/diploma.svg' alt='' />
                </div>
                <div className='main-rules-title'>Продукция</div>
                <div className='main-rules-text'>
                  Продукция провереная сертификатами качества
                </div>
              </div>
              <div className='main-rules-div'></div>
              <div className='main-rules-i'>
                <div className='main-rules-img'>
                  <img src='../public/images/icons/call-center.svg' alt='' />
                </div>
                <div className='main-rules-title'>Call-center</div>
                <div className='main-rules-text'>
                  По вопросам звоните по нашему номеру
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  products: state.product.products,
  new: state.product.new,
  categories: state.product.categories,
});

export default connect(
  mapStateToProps,
  {
    getProducts,
    getNewProducts,
    clearProducts,
    startLoad,
    endLoad,
  },
)(Main);
