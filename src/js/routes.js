import React, { Fragment, lazy, Suspense } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Helmet from 'react-helmet';

import Main from './containers/main';
import Header from './containers/header';
import Product from './containers/product';
import Products from './containers/goods';

import LoadingBar from './utils/loading';
import Category from './containers/category';
import Footer from './components/footer';

const Routes = () => (
  <Fragment>
    <LoadingBar />
    <Route component={Header} />
    <Switch>
      <Route exact path='/' component={Main} />
      <Route path='/good/:id' component={Product} />
      <Route path='/goods/:category' component={Category} />
      <Route path='/goods' component={Products} />
    </Switch>

    <Route component={Footer} />
  </Fragment>
);

export default Routes;
