import React, { Fragment, Component, useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Input, Tooltip, Icon, Empty } from 'antd';
import 'antd/lib/input/style/index.css';
import 'antd/lib/tooltip/style/index.css';
import 'antd/lib/icon/style/index.css';

import { showCart, addCart, register, login } from '../../reducers/authReducer';

import './cart.css';

class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: props.auth.user,
    };
    this.save = this.save.bind(this);
  }

  save() {
    var widget = new cp.CloudPayments();

    widget.charge(
      {
        // options
        publicId: 'test_api_00000000000000000000001', //id из личного кабинета
        description: 'Оплата заказа', //назначение
        amount: this.props.auth.price, //сумма
        currency: 'KZT', //валюта
        invoiceId: '32123', //номер заказа  (необязательно)
        accountId: this.props.auth.user.email, //идентификатор плательщика (необязательно)
        data: {
          myProp: 'myProp value', //произвольный набор параметров
        },
        skin: 'modern',
      },
      options => {
        console.log(options);
        // success
        //действие при успешной оплате
      },
      (reason, options) => {
        console.log(reason);
        // fail
        //действие при неуспешной оплате
      },
    );
  }

  componentDidMount() {}

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.auth.user) {
      return {
        user: nextProps.auth.user,
      };
    }
    return {};
  }

  render() {
    if (this.props.auth.openCart && this.props.auth.user) {
      return (
        <>
          <div className='auth'>
            <div
              className='auth-bg'
              onClick={() => {
                this.props.showCart(false);
              }}
            ></div>
            <div className='auth-inner cart-inner fade'>
              <div className='auth-title'>Корзина</div>
              {this.props.auth.cart.length > 0 ? (
                <div className={'cart-items'}>
                  <div className='cart-i'>
                    <div className='cart-count'>Количество</div>
                    <div className='cart-name'>Наименованние</div>
                    <div className='cart-price'>
                      <div>Цена</div>
                    </div>
                  </div>
                  {this.props.auth.cart.map((i, k) => {
                    return (
                      <div className='cart-i'>
                        <div className='cart-count'>{i.count} шт</div>
                        <div className='cart-name'>{i.product.name}</div>
                        <div className='cart-price'>
                          <div>{i.product.price} ₸</div>
                          <div>{i.product.price * i.count} ₸</div>
                        </div>
                      </div>
                    );
                  })}
                  <div className='cart-i'>
                    <div className='cart-count'>{this.props.auth.count} шт</div>
                    <div className='cart-name'></div>
                    <div className='cart-price'>
                      <div>{this.props.auth.price} ₸</div>
                    </div>
                  </div>
                </div>
              ) : (
                <div className='cartEmptyy'>Корзина пуста</div>
              )}
              <div className='auth-form'>
                <div class='auth-button' onClick={this.save.bind(this)}>
                  Оформить заказ
                </div>
              </div>
            </div>
          </div>
        </>
      );
    }
    return null;
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  {
    addCart,
    showCart,
    register,
    login,
  },
)(Cart);
