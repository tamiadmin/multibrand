import React, { Fragment, Component, useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Input, Tooltip, Icon } from 'antd';
import 'antd/lib/input/style/index.css';
import 'antd/lib/tooltip/style/index.css';
import 'antd/lib/icon/style/index.css';

import {
  showProfile,
  addCart,
  register,
  login,
  logout,
} from '../../reducers/authReducer';

import './profile.css';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reg: false,
      user: props.auth.user,
    };
  }

  changeRForm(e, p) {
    this.setState({
      user: {
        ...this.state.user,
        [p]: e.target.value,
      },
    });
  }

  save() {
    this.props.changeUser(this.state.logform);
  }

  componentDidMount() {}

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.auth.user) {
      return {
        user: nextProps.auth.user,
      };
    }
    return {};
  }

  render() {
    if (this.props.auth.openProfile && this.props.auth.user) {
      return (
        <>
          <div className='auth'>
            <div
              className='auth-bg'
              onClick={() => {
                this.props.showProfile(false);
                this.setState({ reg: false });
              }}
            ></div>
            <div className='auth-inner fade'>
              <div className='auth-title'>Профиль</div>
              <div className='auth-form'>
                <div className='auth-input'>
                  <Input
                    placeholder='Почта'
                    prefix={
                      <Icon type='mail' style={{ color: 'rgba(0,0,0,.25)' }} />
                    }
                    onChange={e => this.changeRForm(e, 'email')}
                    value={this.state.user.email}
                    suffix={
                      <Tooltip title='Проверьте вашу почту это важно'>
                        <Icon
                          type='info-circle'
                          style={{ color: 'rgba(0,0,0,.45)' }}
                        />
                      </Tooltip>
                    }
                  />
                </div>
                <div className='auth-input'>
                  <Input
                    placeholder='Номер телефона'
                    value={this.state.user.phone_number}
                    prefix={
                      <Icon type='phone' style={{ color: 'rgba(0,0,0,.25)' }} />
                    }
                    onChange={e => this.changeRForm(e, 'phone_number')}
                    suffix={
                      <Tooltip title='Важно что бы связаться с вами'>
                        <Icon
                          type='info-circle'
                          style={{ color: 'rgba(0,0,0,.45)' }}
                        />
                      </Tooltip>
                    }
                  />
                </div>
                <div className='auth-input'>
                  <Input
                    placeholder='Имя'
                    value={this.state.user.first_name}
                    prefix={
                      <Icon
                        type='contacts'
                        style={{ color: 'rgba(0,0,0,.25)' }}
                      />
                    }
                    onChange={e => this.changeRForm(e, 'first_name')}
                    suffix={
                      <Tooltip title='Важно что бы связаться с вами'>
                        <Icon
                          type='info-circle'
                          style={{ color: 'rgba(0,0,0,.45)' }}
                        />
                      </Tooltip>
                    }
                  />
                </div>
                <div className='auth-input'>
                  <Input
                    placeholder='Фамилия'
                    value={this.state.user.last_name}
                    prefix={
                      <Icon
                        type='contacts'
                        style={{ color: 'rgba(0,0,0,.25)' }}
                      />
                    }
                    onChange={e => this.changeRForm(e, 'last_name')}
                    suffix={
                      <Tooltip title='Важно что бы связаться с вами'>
                        <Icon
                          type='info-circle'
                          style={{ color: 'rgba(0,0,0,.45)' }}
                        />
                      </Tooltip>
                    }
                  />
                </div>

                <div class='auth-button' onClick={this.save.bind(this)}>
                  Сохранить
                </div>
                <div
                  class='auth-reg-button'
                  onClick={() => this.props.logout()}
                >
                  Выйти
                </div>
              </div>
            </div>
          </div>
        </>
      );
    }
    return null;
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  {
    addCart,
    showProfile,
    register,
    logout,
    login,
  },
)(Profile);
