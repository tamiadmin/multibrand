import React, { Fragment, Component, useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { addCart } from '../../reducers/authReducer';
import { urlApi } from '../../urls';

class ProductItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 1,
      added: false,
    };

    this.changeCount = this.changeCount.bind(this);
  }

  componentDidMount() {}

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.cart.length > 0) {
      if (nextProps.cart.find(o => o.product.id === nextProps.product.id)) {
        return {
          added: true,
        };
      }
    }
    return {};
  }

  changeCount(e) {
    if (e) {
      this.setState(prevState => ({
        count: prevState.count + 1,
      }));
    } else {
      if (this.state.count > 1) {
        this.setState(prevState => ({
          count: prevState.count - 1,
        }));
      }
    }
  }

  addCart() {
    this.setState({});
    this.props.addCart({
      count: this.state.count,
      product: this.props.product,
    });
  }

  render() {
    const i = this.props.product;

    console.log(i.image ? i.image.substring(0, 1) : null);

    return (
      <Fragment>
        <div
          className={
            this.props.width ? 'main-list-i main-list-i-goods' : 'main-list-i'
          }
        >
          <div className='main-list-i-inner'>
            <Link to={'/good/' + i.id}>
              <div className='main-list-img'>
                <img
                  src={
                    i.image
                      ? i.image.substring(0, 1) !== '/'
                        ? i.image
                        : urlApi + i.image
                      : null
                  }
                  alt=''
                />
              </div>
              <div className='main-list-i-content'>
                <div className='main-list-i-title'>{i.name}</div>
                {/* <div className='main-list-i-info'>{i.desc}</div> */}
                <div className='main-list-i-price'>
                  <div className='main-list-i-price-actual'>{i.price} ₸</div>
                </div>
              </div>
            </Link>

            <div className='main-list-i-buy'>
              <div className='main-list-i-buy-count'>
                <div
                  className='main-list-i-buy-count-minus'
                  onClick={() => this.changeCount(false)}
                >
                  -
                </div>
                <div className='main-list-i-buy-count-num'>
                  {this.state.count}
                </div>
                <div
                  className='main-list-i-buy-count-plus'
                  onClick={() => this.changeCount(true)}
                >
                  +
                </div>
              </div>
              {this.state.added ? (
                <div className='main-list-i-buy-button main-list-i-buy-button-active'>
                  В корзине
                </div>
              ) : (
                <div
                  className='main-list-i-buy-button'
                  onClick={this.addCart.bind(this)}
                >
                  В корзину
                </div>
              )}
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  cart: state.auth.cart,
});

export default connect(
  mapStateToProps,
  {
    addCart,
  },
)(ProductItem);
