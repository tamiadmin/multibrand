import React, { Fragment, Component, useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Input, Tooltip, Icon } from 'antd';

import './footer.css';

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        <div className='footer'>
          <div className='container'>
            <div className='footer-inner'>
              <div className='footer-i'>
                <div className='footer-title'>Интрнет-магазин</div>
                <div className='footer-links'>
                  <Link to='/'>Главная</Link>
                  <Link to='/delivery'>Доставка</Link>
                  <Link to='/about'>О компании</Link>
                  <Link to='/feeds'>Отзывы</Link>
                  <Link to='/contacts'>Контакты</Link>
                </div>
              </div>
              <div className='footer-i'>
                <div className='footer-title'>Категории</div>
                <div className='footer-links'>
                  {this.props.categories.map((i, k) => {
                    return <Link to={'/goods/' + i.id}>{i.name}</Link>;
                  })}
                </div>
              </div>
              <div className='footer-i'>
                <div className='footer-title'>О компании</div>
                <div className='footer-links'>
                  <div className='footer-about'>
                    Multibrand это компании по доставке товаров в любую точку
                    Алматы
                  </div>
                </div>
              </div>
              <div className='footer-i'>
                <div className='footer-title'>Контакты</div>
                <div className='footer-links'>
                  <div className='footer-contacts'>
                    <div className='footer-contact'>
                      <a href='tel:87473039758'>+7(747)321-12-21</a>
                    </div>
                    <div className='footer-contact'>
                      <a href='mailto:multibrand_office@mail.ru'>
                        multibrand_office@mail.ru
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.product.categories,
});

export default connect(mapStateToProps)(Footer);
