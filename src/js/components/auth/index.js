import React, { Fragment, Component, useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Input, Tooltip, Icon } from 'antd';
import 'antd/lib/input/style/index.css';
import 'antd/lib/tooltip/style/index.css';
import 'antd/lib/icon/style/index.css';

import { showAuth, addCart, register, login } from '../../reducers/authReducer';

import './auth.css';

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reg: false,
      logform: {
        email: '',
        password: '',
      },
      regForm: {
        email: '',
        password: '',
        phone_number: '',
        first_name: '',
        last_name: '',
        confirm_password: '',
      },
    };
  }

  changeLForm(e, p) {
    this.setState({
      logform: {
        ...this.state.logform,
        [p]: e.target.value,
      },
    });
  }

  changeRForm(e, p) {
    this.setState(
      {
        regForm: {
          ...this.state.regForm,
          [p]: e.target.value,
        },
      },
      console.log(e.target.value),
    );
  }

  login() {
    this.props.login(this.state.logform);
  }
  register() {
    this.props.register(this.state.regForm);
  }

  componentDidMount() {}

  static getDerivedStateFromProps(nextProps, prevState) {
    console.log(prevState);
    return {};
  }

  render() {
    if (this.props.auth.openAuth) {
      if (this.state.reg) {
        return (
          <>
            <div className='auth'>
              <div
                className='auth-bg'
                onClick={() => {
                  this.props.showAuth(false);
                  this.setState({ reg: false });
                }}
              ></div>
              <div className='auth-inner fade'>
                <div className='auth-title'>Регистрация</div>
                <div className='auth-form'>
                  <div className='auth-input'>
                    <Input
                      placeholder='Почта'
                      prefix={
                        <Icon
                          type='mail'
                          style={{ color: 'rgba(0,0,0,.25)' }}
                        />
                      }
                      onChange={e => this.changeRForm(e, 'email')}
                      suffix={
                        <Tooltip title='Проверьте вашу почту это важно'>
                          <Icon
                            type='info-circle'
                            style={{ color: 'rgba(0,0,0,.45)' }}
                          />
                        </Tooltip>
                      }
                    />
                  </div>
                  <div className='auth-input'>
                    <Input.Password
                      placeholder='Пароль'
                      prefix={
                        <Icon
                          type='user'
                          style={{ color: 'rgba(0,0,0,.25)' }}
                        />
                      }
                      onChange={e => this.changeRForm(e, 'password')}
                      suffix={
                        <Tooltip title='Проверьте вашу пароль это важно'>
                          <Icon
                            type='info-circle'
                            style={{ color: 'rgba(0,0,0,.45)' }}
                          />
                        </Tooltip>
                      }
                    />
                  </div>
                  <div className='auth-input'>
                    <Input.Password
                      placeholder='Подвердите пароль'
                      prefix={
                        <Icon
                          type='user'
                          style={{ color: 'rgba(0,0,0,.25)' }}
                        />
                      }
                      onChange={e => this.changeRForm(e, 'confirm_password')}
                      suffix={
                        <Tooltip title='Пароли должны совпадать'>
                          <Icon
                            type='info-circle'
                            style={{ color: 'rgba(0,0,0,.45)' }}
                          />
                        </Tooltip>
                      }
                    />
                  </div>
                  <div className='auth-input'>
                    <Input
                      placeholder='Номер телефона'
                      prefix={
                        <Icon
                          type='phone'
                          style={{ color: 'rgba(0,0,0,.25)' }}
                        />
                      }
                      onChange={e => this.changeRForm(e, 'phone_number')}
                      suffix={
                        <Tooltip title='Важно что бы связаться с вами'>
                          <Icon
                            type='info-circle'
                            style={{ color: 'rgba(0,0,0,.45)' }}
                          />
                        </Tooltip>
                      }
                    />
                  </div>
                  <div className='auth-input'>
                    <Input
                      placeholder='Имя'
                      prefix={
                        <Icon
                          type='contacts'
                          style={{ color: 'rgba(0,0,0,.25)' }}
                        />
                      }
                      onChange={e => this.changeRForm(e, 'first_name')}
                      suffix={
                        <Tooltip title='Важно что бы связаться с вами'>
                          <Icon
                            type='info-circle'
                            style={{ color: 'rgba(0,0,0,.45)' }}
                          />
                        </Tooltip>
                      }
                    />
                  </div>
                  <div className='auth-input'>
                    <Input
                      placeholder='Фамилия'
                      prefix={
                        <Icon
                          type='contacts'
                          style={{ color: 'rgba(0,0,0,.25)' }}
                        />
                      }
                      onChange={e => this.changeRForm(e, 'last_name')}
                      suffix={
                        <Tooltip title='Важно что бы связаться с вами'>
                          <Icon
                            type='info-circle'
                            style={{ color: 'rgba(0,0,0,.45)' }}
                          />
                        </Tooltip>
                      }
                    />
                  </div>

                  <div class='auth-button' onClick={this.register.bind(this)}>
                    Регистрация
                  </div>
                  <div
                    class='auth-reg-button'
                    onClick={() =>
                      this.setState({
                        reg: false,
                      })
                    }
                  >
                    Войти
                  </div>
                </div>
              </div>
            </div>
          </>
        );
      } else {
        return (
          <Fragment>
            <div className='auth'>
              <div
                className='auth-bg'
                onClick={() => this.props.showAuth(false)}
              ></div>
              <div className='auth-inner fade'>
                <div className='auth-title'>Войти</div>
                <div className='auth-form'>
                  <div className='auth-input'>
                    <Input
                      placeholder='Почта'
                      prefix={
                        <Icon
                          type='mail'
                          style={{ color: 'rgba(0,0,0,.25)' }}
                        />
                      }
                      onChange={e => this.changeLForm(e, 'email')}
                      suffix={
                        <Tooltip title='Проверьте вашу почту это важно'>
                          <Icon
                            type='info-circle'
                            style={{ color: 'rgba(0,0,0,.45)' }}
                          />
                        </Tooltip>
                      }
                    />
                  </div>
                  <div className='auth-input'>
                    <Input.Password
                      placeholder='Пароль'
                      prefix={
                        <Icon
                          type='user'
                          style={{ color: 'rgba(0,0,0,.25)' }}
                        />
                      }
                      onChange={e => this.changeLForm(e, 'password')}
                      suffix={
                        <Tooltip title='Проверьте вашу пароль это важно'>
                          <Icon
                            type='info-circle'
                            style={{ color: 'rgba(0,0,0,.45)' }}
                          />
                        </Tooltip>
                      }
                    />
                  </div>
                  <div class='auth-button' onClick={this.login.bind(this)}>
                    Войти
                  </div>
                  <div
                    class='auth-reg-button'
                    onClick={() =>
                      this.setState({
                        reg: true,
                      })
                    }
                  >
                    Регистрация
                  </div>
                </div>
              </div>
            </div>
          </Fragment>
        );
      }
    }
    return null;
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  {
    addCart,
    showAuth,
    register,
    login,
  },
)(Auth);
